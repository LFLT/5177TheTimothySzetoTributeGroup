package gitClimateData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.Month;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class ClimateData {

	static List<Locations> locationsArray = new ArrayList<Locations>();
	static Scanner console = new Scanner(System.in);

	public static void main(String[] args) throws FileNotFoundException {

		System.out.println("Loading...");
		// Background work
		readData();

		loadMinMax();
		loadAverages();

		// User Side
		mainMenu();

		console.close();
	}

	private static void loadAverages() {
		
		
		// for every location
		for(Locations loc : locationsArray){
			
			float rain = 0;
			float maxTemp = 0;
			float minTemp = 0;
			float sunH = 0;
			int counter = 0;
			
			// for every instance 
			for (WeatherData wd : loc.weatherArray){
				
				String currRain = wd.getRainMM();
				String currMaxTemp = wd.getMaxTemp();
				String currMinTemp = wd.getMinTemp();
				String currSunH = wd.getSunHours();
				
				if(currRain != null)
				rain += Float.parseFloat(currRain);
				
				if(currMaxTemp != null)
				maxTemp += Float.parseFloat(currMaxTemp);
				
				if(currMinTemp != null)
				minTemp += Float.parseFloat(currMinTemp);
				
				if(currSunH != null)
				sunH += Float.parseFloat(currSunH);
				
				counter++;
				
			}
			loc.setAvgRain(rain / counter);
			loc.setAvgMaxTemp(maxTemp / counter);
			loc.setAvgMinTemp(minTemp / counter);
			loc.setAvgSunH(sunH / counter);
			
		}
		
	}

	/*
	 * Prints main options for the user.
	 */
	public static void mainMenu() {
		String choice = " ";
		do {
			System.out.println("\n---- Please choose a feature that you would like to use ----");
			System.out.println("1. List all locations we have data for");
			System.out.println("2. List all weather observations for a chosen location");
			System.out.println("3. List weather observations for a chosen date");
			System.out.println("4. List Max/Min temp for a chosen location");
			System.out.println("5. Get Max/Min location data");
			System.out.println("6. List of all locations in chronological order");
			System.out.println("Q - Quit");

			choice = console.nextLine();
			switch (choice) {

			// Feature A
			case "1": {
				printLocationNames();
				break;
			}
			// Feature B
			case "2": {
				locationWeatherObservations();
				break;
			}
			// Feature C
			case "3": {
				dateObservations();
				break;
			}
			// Feature D
			case "4": {
				maxMinRecData();
				break;
			}
			// Feature E & F
			case "5": {

				getTempDetails();
				break;
			}
			// Feature G
			case "6": {
				chronologicalSort();
				break;
			}
			default: {
				break;
			}

			}
		} while (!choice.equalsIgnoreCase("q"));
	}

	///////////////
	// Feature A //
	///////////////

	/*
	 * Prints location names
	 */
	private static void printLocationNames() {

		System.out.println("\nWeather Locations:");

		for (Locations locArray : locationsArray)
			System.out.println(locArray.getName());

		System.out.println();

	}

	///////////////
	// Feature B //
	///////////////

	/*
	 * Method prompts the user to choose a location. returns the location
	 */
	private static Locations chooseLocation() {

		// User Side
		System.out.println("Choose Your Location: ");
		printLocationNames();

		// Back end
		String choice;
		do {
			choice = console.nextLine();

			Locations loc = new Locations();
			loc.setName(choice);

			List<Locations> list = Search.searchList(Locations.loc_name, locationsArray, loc);

			if (!list.isEmpty())
				return list.get(0);

			System.out.println("Invalid Location: " + choice);
		} while (true);
	}

	/**
	 * Allows the user to view all weather observations for a location, ordered
	 * by date and can be in ascending or descending *
	 */
	public static void locationWeatherObservations() {

		Locations chosenLoc = chooseLocation();

		System.out.println("Choose Order:[ASC/DSC]");
		String order = console.nextLine();
		Comparator<WeatherData> c = null;

		switch (order.toUpperCase()) {
		case "ASC": {
			c = WeatherData.date_asc;
			break;
		}
		case "DSC": {
			c = WeatherData.date_dsc;
			break;
		}
		default:
			System.out.println("Bad Input!");
			break;
		}
		if (c != null) {
			Sort.quickSort(chosenLoc.weatherArray, c, 0, chosenLoc.weatherArray.size() - 1);
			printArray(chosenLoc.weatherArray);
			System.out.println(chosenLoc.printAverages());
		}

	}

	///////////////
	// Feature C //
	///////////////

	private static void dateObservations() {
		// user side
		System.out.println("Please choose the year you would like to view:");
		String year = console.nextLine();
		System.out.println("Please choose the month you would like to view:");
		String month = console.nextLine();

		month = Integer.toString(Month.valueOf(month.toUpperCase()).getValue());

		WeatherData wd = new WeatherData();
		wd.setYear(year);
		wd.setMonth(month);

		List<WeatherData> list = Search.searchAll(WeatherData.date_asc, wd);

		printArray(list);
	}

	
	///////////////
	// Feature D //
	///////////////
	
	public static void maxMinRecData(){
		System.out.println("---- Choose data and location to view data ----");
		System.out.println("\n1. Temperature");
		System.out.println("2. Rainfall");
		System.out.println("3. Sun Hours");
		String choice = console.nextLine();
		Locations loc = chooseLocation();
		
		switch (choice) {

		case "1": {
			minMaxRecordedTemps(loc);
			break;
		}
		case "2": {
			minMaxRecordedRain(loc);
			break;
		}
		case "3": {
			minMaxRecordedSun(loc);
			break;
		}
		}
	}
	
	/**
	 * Prints out the min/max temp or the chosen location
	 */
	public static void minMaxRecordedTemps(Locations chosenLoc) {

		System.out.println("Maximum Temperature: " + chosenLoc.getHighestTemp());
		System.out.println("Minimum Temperature: " + chosenLoc.getLowestTemp());
	}
	
	/**
	 * Prints out the min/max temp or the chosen location
	 */
	public static void minMaxRecordedRain(Locations chosenLoc) {

		System.out.println("Maximum Rainfall: " + chosenLoc.getHighestRain());
		System.out.println("Minimum Rainfall: " + chosenLoc.getLowestRain());
	}

	/**
	 * Prints out the min/max temp or the chosen location
	 */
	public static void minMaxRecordedSun(Locations chosenLoc) {

		System.out.println("Maximum SunH: " + chosenLoc.getHighestSunH());
		System.out.println("Minimum SunH: " + chosenLoc.getLowestSunH());
	}
	
	/*
	 * Holds the methods for locating the Min/Max for each datatype.
	 */
	private static void loadMinMax() {
		findMinMaxTemp();
		findMinMaxRain();
		findMinMaxSunH();
		findMinDate();
		System.out.println("\n\n\n\n\n");
	}

	// Enums to decide which data point to use
	private enum DataPoints {
		TMP, SUNH, RAIN, DATE;
	}

	private static void findMinDate() {

		for (Locations loc : locationsArray) {

			WeatherData max = Search.searchMax(WeatherData.date_min, loc.weatherArray);
			WeatherData min = Search.searchMin(WeatherData.date_min, loc.weatherArray);
			setMinMaxWdVal(DataPoints.DATE, loc, max, min);
		}
		findOverallMinDate();
	}

	private static void findOverallMinDate() {

		Locations max = Search.searchMax(Locations.date_min, locationsArray);
		Locations min = Search.searchMin(Locations.date_min, locationsArray);
		setMinMaxLocVal(DataPoints.DATE, max, min);
	}

	/**
	 * Loops through all of the locations and sets the highest and lowest rain
	 * value.
	 */
	private static void findMinMaxRain() {

		for (Locations loc : locationsArray) {
			WeatherData max = Search.searchMax(WeatherData.rainfall, loc.weatherArray);
			WeatherData min = Search.searchMin(WeatherData.rainfall_min, loc.weatherArray);
			setMinMaxWdVal(DataPoints.RAIN, loc, max, min);
		}
		// repeats this process but for all locations, rather than weather data
		findOverallMinMaxRain();
	}

	private static void findOverallMinMaxRain() {

		Locations max = Search.searchMax(Locations.maxrain, locationsArray);
		Locations min = Search.searchMin(Locations.minrain, locationsArray);
		setMinMaxLocVal(DataPoints.RAIN, max, min);
	}

	/**
	 * Loops through all of the locations and sorts each instance based on the
	 * maximum temperature this then calls the method which sets the highest
	 * temp
	 */
	private static void findMinMaxTemp() {

		for (Locations loc : locationsArray) {
			WeatherData max = Search.searchMax(WeatherData.maxtemp, loc.weatherArray);
			WeatherData min = Search.searchMin(WeatherData.min_temp, loc.weatherArray);
			setMinMaxWdVal(DataPoints.TMP, loc, max, min);
		}
		findOverallMinMaxTemp();
	}

	private static void findOverallMinMaxTemp() {

		Locations max = Search.searchMax(Locations.maxtemp, locationsArray);
		Locations min = Search.searchMin(Locations.mintemp, locationsArray);
		setMinMaxLocVal(DataPoints.TMP, max, min);

	}

	/**
	 * Loops through the locations array, sorting the data by its sunhours
	 * method then calls the function to work out the highest and lowest value.
	 */
	private static void findMinMaxSunH() {

		for (Locations loc : locationsArray) {

			WeatherData max = Search.searchMax(WeatherData.sunH, loc.weatherArray);
			WeatherData min = Search.searchMin(WeatherData.sunH_min, loc.weatherArray);
			setMinMaxWdVal(DataPoints.SUNH, loc, max, min);
		}
		findOverallMinMaxSunH();
	}

	/**
	 * This method works out the highest and lowest sun hours value out of all locations
	 * 
	 */
	private static void findOverallMinMaxSunH() {

		Locations max = Search.searchMax(Locations.maxsunh, locationsArray);
		Locations min = Search.searchMin(Locations.minsunh, locationsArray);
		setMinMaxLocVal(DataPoints.SUNH, max, min);
	}

	/**
	 * This method sets the max and min value for each array of weather data
	 * based on the enum provided.
	 * 
	 * @param caseSwitch
	 *            this tells the programme which data we are looking for
	 * @param loc
	 *            the location that we are looking to get the data for
	 */
	public static void setMinMaxWdVal(DataPoints caseSwitch, Locations loc, WeatherData max, WeatherData min) {

		switch (caseSwitch) {

		case RAIN: {
			loc.setHighestRain(max.getRainMM());
			loc.setLowestRain(min.getRainMM());
			break;
		}
		case SUNH: {
			loc.setHighestSunH(max.getSunHours());
			loc.setLowestSunH(min.getSunHours());
			break;
		}
		case TMP: {
			loc.setHighestTemp(max.getMaxTemp());
			loc.setLowestTemp(min.getMinTemp());
			break;
		}
		case DATE: {
			loc.setLowestYear(min.getYear());
			break;
		}
		default:
			break;
		}

	}

	///////////////////
	// Feature E & F //
	///////////////////

	/**
	 * Displays the menu for getting the highest and lowest overall values based
	 * on the user selection
	 */
	private static void getTempDetails() {

		System.out.println("Choose data: ");
		System.out.println("1. Rainfall");
		System.out.println("2. Sun Hours");
		System.out.println("3. Temperature");
		String choice = console.nextLine();

		switch (choice) {

		case "1": {
			System.out.println("Maximum Rainfall: " + Locations.getOverallTopRain().getHighestRain());
			System.out.println("        Location: " + Locations.getOverallTopRain().getName());
			System.out.println("\nMinimum Rainfall: " + Locations.getOverallBotRain().getLowestRain());
			System.out.println("        Location: " + Locations.getOverallBotRain().getName());
			break;
		}
		case "2": {
			System.out.println("Maximum Sun Hours: " + Locations.getOverallTopSunH().getHighestSunH());
			System.out.println("         Location: " + Locations.getOverallTopSunH().getName());
			System.out.println("\nMinimum Sun Hours: " + Locations.getOverallBotSunH().getLowestSunH());
			System.out.println("         Location: " + Locations.getOverallBotSunH().getName());
			break;
		}
		case "3": {
			System.out.println("Maximum Temperature: " + Locations.getOverallTopTemp().getHighestTemp());
			System.out.println("           Location: " + Locations.getOverallTopTemp().getName());
			System.out.println("\nMinimum Temperature: " + Locations.getOverallBotTemp().getLowestTemp());
			System.out.println("           Location: " + Locations.getOverallBotTemp().getName());
			break;
		}
		}

	}

	/**
	 * Sets the location with the highest and lowest values based on the data type
	 * 
	 * @param dataType the type of data which needs to be set
	 * @param max the location with the highest result
	 * @param min the location with the lowest result
	 */
	public static void setMinMaxLocVal(DataPoints dataType, Locations max, Locations min) {

		switch (dataType) {

		case RAIN: {
			Locations.setOverallTopRain(max);
			Locations.setOverallBotRain(min);
			break;
		}
		case SUNH: {
			Locations.setOverallTopSunH(max);
			Locations.setOverallBotSunH(min);
			break;
		}
		case TMP: {
			Locations.setOverallTopTemp(max);
			Locations.setOverallBotTemp(min);
			break;
		}
		default:
			break;
		}
	}

	///////////////
	// Feature G //
	///////////////

	/**
	 * This method sorts every array based on its date and works out the
	 * earliest year the method then sorts every location by its earliest year
	 * and prints out the results.
	 */
	private static void chronologicalSort() {

		Sort.quickSort(locationsArray, Locations.date_min, 0, locationsArray.size() - 1);

		for (Locations loc : locationsArray) {
			System.out.println(loc.getLowestYear() + " \t" + loc.getName());
		}
	}

	/////////////
	// Methods //
	/////////////

	/**
	 * Uses the objects toString method to print out the relevant data.
	 * @param <T>
	 * 
	 * @param data
	 */
	static <T> void printArray(List<T> data) {

		for (T dat : data){
			System.out.println(dat);
		}
		System.out.println("AREA \tYEAR \tMONTH \tRAIN \tMAXTMP \tMINTMP \tSUNH");
		
	}

	/**
	 * Getter to return the top of the list.
	 * 
	 * @return returns the end position of the location array, requires the
	 *         array to be sorted for use
	 */
	public static Locations getTopLocVal() {
		return locationsArray.get(locationsArray.size() - 1);
	}

	/**
	 * Getter to return the top of the Array list
	 * 
	 * @param loc
	 *            location to get weather data from
	 *            
	 * @return The end of the Array list
	 */
	public static WeatherData getTopWdVal(Locations loc) {
		return loc.weatherArray.get(loc.weatherArray.size() - 1);
	}

	/**
	 * Reads through the resources folder and inserts the data into the location
	 * and weather data object.
	 * 
	 * @throws FileNotFoundException
	 */
	private static void readData() throws FileNotFoundException {

		File folder = new File("resources/");
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {

			FileReader fileReader = new FileReader("resources/" + listOfFiles[i].getName());
			Scanner fileScanner = new Scanner(fileReader);

			// Create locations class
			Locations newLocation = new Locations();

			fileScanner.nextLine();
			String name = listOfFiles[i].getName().substring(0, listOfFiles[i].getName().length() - 8);
			newLocation.setName(name);
			locationsArray.add(newLocation);

			// Skips through the code until it reaches the information needed

			String haltLine = "";
			while (!haltLine.startsWith("yyyy")) {
				haltLine = fileScanner.nextLine().trim();
			}
			fileScanner.nextLine();

			// Reads each line into the weather object constructor to split
			// accordingly.
			String line2 = "";
			while (fileScanner.hasNext()) {
				line2 = fileScanner.nextLine();

				if (line2.trim().toUpperCase().startsWith("SITE CLOSED"))
					break;

				WeatherData weatherObject = new WeatherData(line2);
				weatherObject.setLocation(newLocation);
				newLocation.weatherArray.add(weatherObject);

			}

			// End of reading in,
			fileScanner.close();

		}
	}

}
