package gitClimateData;

import java.text.DateFormatSymbols;
import java.util.Comparator;

public class WeatherData extends Locations {

	/**
	 * This is used to read in the different symbols from the .csv files and put
	 * them into the proper data structures.
	 */
	public enum Symbol {

		HASH("#"), STAR("*"), EMPTY("");
		private final String str;

		private Symbol(String str) {
			this.str = str;
		}

		public String toString() {
			return this.str;
		}

		public static Symbol getFrom(String str) {

			for (Symbol s : Symbol.values())
				if (s.str.equalsIgnoreCase(str))
					return s;

			return Symbol.EMPTY;
		}
	}

	//////////////////
	// Constructors //
	//////////////////

	private String year, monthName, month, maxTemp, minTemp, afDays, rainMM, sunHours;
	private String sensorType = "Sunshine data taken from a Campbell Stokes recorder";
	private Locations location;
	private Symbol symbol;
	private boolean estimated = false;

	public WeatherData() {
	}

	public WeatherData(String fileString) {

		String[] split = fileString.split(",", -1);
		this.year = isNull(split[0]);
		this.month = isNull(split[1]);
		this.monthName = new DateFormatSymbols().getMonths()[getMonthNumber() - 1];
		this.maxTemp = isNull(split[2]);
		this.minTemp = isNull(split[4]);
		this.afDays = isNull(split[6]);
		this.rainMM = isNull(split[8]);
		this.sunHours = isNull(split[10]);
		try {
			this.symbol = Symbol.getFrom(split[11]);
		} catch (ArrayIndexOutOfBoundsException e) {
		}

		if (symbol == Symbol.STAR)
			this.estimated = true;

		if (symbol == Symbol.HASH)
			this.sensorType = "Sunshine data taken from an automatic Kipp & Zonen sensor";

	}

	/////////////
	// Methods //
	/////////////

	/*
	 * Method used while reading in to structures if the data is null
	 * 
	 */
	public String isNull(String str) {

		if (str.equals("---") || str.isEmpty())
			return null;

		return str;
	}

	/**
	 * Generates a String representation of the data
	 */
	public String toString() {

		return this.location.getName().substring(0, 4) + " \t" + this.year + " \t" + this.monthName.substring(0, 3) + "\t"
				+ this.rainMM + " \t" + this.maxTemp + " \t" + this.minTemp+ " \t" + this.sunHours;

	}

	/////////////////////////
	// Getters and Setters //
	/////////////////////////

	public Symbol getSymbol() {
		return symbol;
	}

	public void setSymbol(Symbol symbol) {
		this.symbol = symbol;
	}

	public boolean isEstimated() {
		return estimated;
	}

	public void setEstimated(boolean estimated) {
		this.estimated = estimated;
	}

	public String getSensorType() {
		return sensorType;
	}

	public void setSensorType(String sensorType) {
		this.sensorType = sensorType;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public int getMonthNumber() {
		return Integer.parseInt(getMonth());
	}

	public String getMonthName() {
		return monthName;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getAfDays() {
		return afDays;
	}

	public void setAfDays(String afDays) {
		this.afDays = afDays;
	}

	public String getMaxTemp() {
		return maxTemp;
	}

	public void setMaxTemp(String maxTemp) {
		this.maxTemp = maxTemp;
	}

	public String getMinTemp() {
		return minTemp;
	}

	public void setMinTemp(String minTemp) {
		this.minTemp = minTemp;
	}

	public String getRainMM() {
		return rainMM;
	}

	public void setRainMM(String rainMM) {
		this.rainMM = rainMM;
	}

	public String getSunHours() {
		return sunHours;
	}

	public void setSunHours(String sunHours) {
		this.sunHours = sunHours;
	}

	public String getMonth() {
		return month;
	}

	
	public void setLocation(Locations loc){
		this.location = loc;
	}
	
	public Locations getLocations(){
		return location;
	}
	

	/////////////////
	// Comparators //
	/////////////////

	/*
	 * Compares the months if the years are equal.
	 */
	public static final Comparator<WeatherData> date_asc = new Comparator<WeatherData>() {

		public int compare(WeatherData item1, WeatherData item2) {

			if (item1.year.equals(item2.year)) {
				return Locations.compareNum(item1.month, item2.month);
			} else {
				return Locations.compareNum(item1.year, item2.year);
			}

		}
	};

	public static final Comparator<WeatherData> date_dsc = new Comparator<WeatherData>() {
		public int compare(WeatherData item1, WeatherData item2) {

			if (item1.year.equals(item2.year)) {
				return Locations.compareNum(item2.month, item1.month);
			} else {
				return Locations.compareNum(item2.year, item1.year);
			}
		}
	};

	public static final Comparator<WeatherData> date_min = new Comparator<WeatherData>() {
		public int compare(WeatherData item1, WeatherData item2) {

			if (item1.year.equals(item2.year)) {
				return Locations.compareNumNull(item1.month, item2.month);
			} else {
				return Locations.compareNumNull(item1.year, item2.year);
			}
		}
	};

	public static final Comparator<WeatherData> maxtemp = new Comparator<WeatherData>() {

		public int compare(WeatherData item1, WeatherData item2) {

			return Locations.compareFloat(item1.maxTemp, item2.maxTemp);
		}
	};

	public static final Comparator<WeatherData> maxtemp_min = new Comparator<WeatherData>() {

		public int compare(WeatherData item1, WeatherData item2) {

			return Locations.compareFloatNull(item1.maxTemp, item2.maxTemp);
		}
	};

	public static final Comparator<WeatherData> mintemp = new Comparator<WeatherData>() {

		public int compare(WeatherData item1, WeatherData item2) {
			return Locations.compareFloat(item1.minTemp, item2.minTemp);
		}
	};

	public static final Comparator<WeatherData> min_temp = new Comparator<WeatherData>() {

		public int compare(WeatherData item1, WeatherData item2) {

			return Locations.compareFloatNull(item1.minTemp, item2.minTemp);
		}
	};

	public static final Comparator<WeatherData> rainfall = new Comparator<WeatherData>() {

		public int compare(WeatherData item1, WeatherData item2) {
			return Locations.compareFloat(item1.rainMM, item2.rainMM);
		}
	};

	public static final Comparator<WeatherData> rainfall_min = new Comparator<WeatherData>() {

		public int compare(WeatherData item1, WeatherData item2) {
			return Locations.compareFloatNull(item1.rainMM, item2.rainMM);
		}

	};

	public static final Comparator<WeatherData> sunH = new Comparator<WeatherData>() {

		public int compare(WeatherData item1, WeatherData item2) {

			return Locations.compareFloat(item1.sunHours, item2.sunHours);

		}
	};

	public static final Comparator<WeatherData> sunH_min = new Comparator<WeatherData>() {

		public int compare(WeatherData item1, WeatherData item2) {

			return Locations.compareFloatNull(item1.sunHours, item2.sunHours);
		}

	};

}
