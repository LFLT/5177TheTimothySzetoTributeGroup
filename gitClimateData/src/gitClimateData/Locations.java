package gitClimateData;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Locations {

	// Highest / lowest data for each weather point
	private String name, highestTemp, lowestTemp, highestRain, lowestRain, highestSunH, lowestSunH, lowestYear, avgRain, avgSun, avgMaxTemp, avgMinTemp;
	

	// Overall Highest / Lowest data
	private static Locations overallTopTemp, overallBotTemp, overallTopRain, overallBotRain, overallTopSunH,
			overallBotSunH;

	public List<WeatherData> weatherArray = new ArrayList<>();

	//////////////////
	// Constructors //
	//////////////////

	/*
	 * Default Constructor For only applying Getters / Setters
	 */
	public Locations() {
	}

	/*
	 * Alternative Constructor For Reading in
	 */
	public Locations(String name) {
		this.name = name;
	}

	/////////////
	// Methods //
	/////////////

	/**
	 * Generates a String representation of the data
	 */
	public String toString() {
		String text = "  Location:	" + this.name + "\n";
		return text;
	}

	/**
	 * Used with the comparators to compare String values
	 * 
	 * @param num1
	 *            the first Int as a string
	 * @param num2
	 *            the second Int as a string
	 * @return the result of comparing the two numbers, -1 is lower, 0 is same
	 *         and 1 is higher.
	 */
	public static int compareNum(String num1, String num2) {

		if (num1 == null && num2 == null)
			return 0;
		if (num2 == null)
			return 1;
		if (num1 == null)
			return -1;

		int numb1 = Integer.parseInt(num1);
		int numb2 = Integer.parseInt(num2);

		return numb1 - numb2;
	}

	/**
	 * Used with the comparators to compare String values
	 * 
	 * @param num1
	 *            the first Int as a string
	 * @param num2
	 *            the second Int as a string
	 * @return the result of comparing the two numbers, -1 is lower, 0 is same
	 *         and 1 is higher.
	 */
	public static int compareString(String num1, String num2) {

		if (num1 == null && num2 == null)
			return 0;
		if (num2 == null)
			return 1;
		if (num1 == null)
			return -1;

		return num1.compareTo(num2);
	}

	public static int compareNumNull(String num1, String num2) {

		if (num1 == null && num2 == null)
			return 0;
		if (num2 == null)
			return -1;
		if (num1 == null)
			return 1;

		int numb1 = Integer.parseInt(num1);
		int numb2 = Integer.parseInt(num2);

		return numb1 - numb2;
	}

	/**
	 * Used with the comparators to compare floats values
	 * 
	 * @param num1
	 *            the first float as a string
	 * @param num2
	 *            the second float as a string
	 * @return the result of comparing the two numbers, -1 is lower, 0 is same
	 *         and 1 is higher.
	 */
	public static int compareFloat(String num1, String num2) {

		if (num1 == null && num2 == null)
			return 0;
		if (num2 == null)
			return 1;
		if (num1 == null)
			return -1;

		Float numb1 = Float.parseFloat(num1);
		Float numb2 = Float.parseFloat(num2);

		return Float.compare(numb1, numb2);
	}

	/**
	 * Used with the comparators to compare and avoid nulls
	 * 
	 * @param num1
	 *            the first float as a string
	 * @param num2
	 *            the second float as a string
	 * @return the result of comparing the two numbers, -1 is lower, 0 is same
	 *         and 1 is higher.
	 */
	public static int compareFloatNull(String num1, String num2) {

		if (num1 == null && num2 == null)
			return 0;
		if (num2 == null)
			return -1;
		if (num1 == null)
			return 1;

		Float numb1 = Float.parseFloat(num1);
		Float numb2 = Float.parseFloat(num2);

		return Float.compare(numb1, numb2);
	}

	/////////////////////////
	// Getters and Setters //
	/////////////////////////

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHighestTemp() {
		return highestTemp;
	}

	public void setHighestTemp(String highestTemp) {
		this.highestTemp = highestTemp;
	}

	public String getLowestTemp() {
		return lowestTemp;
	}

	public void setLowestTemp(String lowestTemp) {
		this.lowestTemp = lowestTemp;
	}

	public String getHighestRain() {
		return highestRain;
	}

	public void setHighestRain(String highestRain) {
		this.highestRain = highestRain;
	}

	public String getLowestRain() {
		return lowestRain;
	}

	public void setLowestRain(String lowestRain) {
		this.lowestRain = lowestRain;
	}

	public String getHighestSunH() {
		return highestSunH;
	}

	public void setHighestSunH(String highestSunH) {
		this.highestSunH = highestSunH;
	}

	public String getLowestSunH() {
		return lowestSunH;
	}

	public void setLowestSunH(String lowestSunH) {
		this.lowestSunH = lowestSunH;
	}

	public static Locations getOverallTopTemp() {
		return overallTopTemp;
	}

	public static void setOverallTopTemp(Locations overallTopTemp) {
		Locations.overallTopTemp = overallTopTemp;
	}

	public static Locations getOverallBotTemp() {
		return overallBotTemp;
	}

	public static void setOverallBotTemp(Locations overallBotTemp) {
		Locations.overallBotTemp = overallBotTemp;
	}

	public static Locations getOverallTopRain() {
		return overallTopRain;
	}

	public static void setOverallTopRain(Locations overallTopRain) {
		Locations.overallTopRain = overallTopRain;
	}

	public static Locations getOverallBotRain() {
		return overallBotRain;
	}

	public static void setOverallBotRain(Locations overallBotRain) {
		Locations.overallBotRain = overallBotRain;
	}

	public static Locations getOverallTopSunH() {
		return overallTopSunH;
	}

	public static void setOverallTopSunH(Locations overallTopSunH) {
		Locations.overallTopSunH = overallTopSunH;
	}

	public static Locations getOverallBotSunH() {
		return overallBotSunH;
	}

	public static void setOverallBotSunH(Locations overallBotSunH) {
		Locations.overallBotSunH = overallBotSunH;
	}

	public String getLowestYear() {
		return lowestYear;
	}

	public void setLowestYear(String lowestYear) {
		this.lowestYear = lowestYear;
	}

	/////////////////
	// Comparators //
	/////////////////

	/**
	 * Comparator used to compare the objects High temps ascending
	 */
	public static final Comparator<Locations> maxtemp = new Comparator<Locations>() {
		public int compare(Locations item1, Locations item2) {

			return compareFloat(item1.highestTemp, item2.highestTemp);
		}
	};

	/**
	 * Comparator used to compare the objects Low temps ascending
	 */
	public static final Comparator<Locations> mintemp = new Comparator<Locations>() {
		public int compare(Locations item1, Locations item2) {

			return compareFloatNull(item1.lowestTemp, item2.lowestTemp);
		}
	};

	/**
	 * Comparator used to compare the objects rain temps ascending
	 */
	public static final Comparator<Locations> maxrain = new Comparator<Locations>() {
		public int compare(Locations item1, Locations item2) {

			return compareFloat(item1.highestRain, item2.highestRain);
		}
	};

	/**
	 * Comparator used to compare the objects min rain ascending
	 */
	public static final Comparator<Locations> minrain = new Comparator<Locations>() {
		public int compare(Locations item1, Locations item2) {

			return compareFloatNull(item1.lowestRain, item2.lowestRain);
		}
	};

	/**
	 * Comparator used to compare the objects max sunH ascending
	 */
	public static final Comparator<Locations> maxsunh = new Comparator<Locations>() {
		public int compare(Locations item1, Locations item2) {

			return compareFloat(item1.highestSunH, item2.highestSunH);
		}
	};

	/**
	 * Comparator used to compare the objects max sunH ascending
	 */
	public static final Comparator<Locations> minsunh = new Comparator<Locations>() {
		public int compare(Locations item1, Locations item2) {
			return compareFloatNull(item1.lowestSunH, item2.lowestSunH);
		}
	};

	/**
	 * Comparator used to compare the date and sort
	 */
	public static final Comparator<Locations> date = new Comparator<Locations>() {
		public int compare(Locations item1, Locations item2) {
			return compareNum(item1.lowestYear, item2.lowestYear);
		}
	};

	/**
	 * Comparator used to compare the date and sort
	 */
	public static final Comparator<Locations> date_min = new Comparator<Locations>() {
		public int compare(Locations item1, Locations item2) {
			return compareNumNull(item1.lowestYear, item2.lowestYear);
		}
	};

	/**
	 * Comparator used to compare the date and sort
	 */
	public static final Comparator<Locations> loc_name = new Comparator<Locations>() {
		public int compare(Locations item1, Locations item2) {

			return compareString(item1.name, item2.name);
		}
	};

	public void setAvgRain(float f) {
		this.avgRain = String.valueOf(f);
	}
	
	public String getAvgRain(){
		return this.avgRain;
	}
	
	public void setAvgSunH(float f) {
		this.avgSun = String.valueOf(f);
	}
	
	public String getAvgSunH(){
		return this.avgSun;
	}
	
	public void setAvgMaxTemp(float f) {
		this.avgMaxTemp = String.valueOf(f);
	}
	
	public String getAvgMaxTemp(){
		return this.avgMaxTemp;
	}
	
	public void setAvgMinTemp(float f) {
		this.avgMinTemp = String.valueOf(f);
	}
	

	public String getAvgMinTemp(){
		return this.avgMinTemp;
	}

	public String printAverages() {
		return "\n ---Averages--- \n"+
			"\n       Rainfall: "+ this.avgRain.substring(0, 5) +
			"\nMax Temperature: "+ this.avgMaxTemp.substring(0, 5) +
			"\nMin Temperature: " + this.avgMinTemp.substring(0, 4) +
			"\n      Sun Hours: " + this.avgSun.substring(0, 5);
		
	}
}
