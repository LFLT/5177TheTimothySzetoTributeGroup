package gitClimateData;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Sort {

	/**
	 * Sorts the array list in order based on the comparator input
	 * 
	 * @param comp
	 *            tells the method how it should sort the data
	 * @param arr
	 *            the array to be sorted
	 */
	public static <T> void insertSort(Comparator<T> comp, List<T> arr) {

		for (int i = 1; i < arr.size(); i++) {

			for (int j = i; j > 0; j--) {

				T objhigher = arr.get(j);
				T objlower = arr.get(j - 1);

				if (comp.compare(objhigher, objlower) < 0) {
					Collections.swap(arr, j, j - 1);
				}
			}
		}
	}

	/**
	 * The quicksort is a recursive method used to divide up the array and sort
	 * it, then put it back together
	 * 
	 * @param arr
	 *            The List to be sorted
	 * @param comp
	 *            Tells the object what attributes to compare
	 * @param low
	 *            The lowest position in the array to be sorted from
	 * @param high
	 *            The highest position in the array to be sorted to.
	 */
	public static <T> void quickSort(List<T> arr, Comparator<T> comp, int low, int high) {

		if (arr == null || arr.size() == 0) {
			return;
		}

		if (low >= high) {
			return;
		}

		if (low < high) {

			int i = low, j = high;

			int middle = low + (high - low) / 2;

			T pivot = arr.get(middle);

			do {
				while (comp.compare(arr.get(i), pivot) < 0) {
					i++;
				}
				while (comp.compare(pivot, arr.get(j)) < 0) {
					j--;

				}
				if (i <= j) {
					T temp = arr.get(i);
					arr.set(i, arr.get(j));
					arr.set(j, temp);
					i++;
					j--;
				}
			} while (i <= j);

			quickSort(arr, comp, low, j);

			quickSort(arr, comp, i, high);

		}

	}

	/**
	 * Used for testing purposes
	 * 
	 * @param comp
	 *            comparator
	 * @param arr
	 *            array
	 */
	public static <T> void bubbleSort(Comparator<T> comp, List<T> arr) {

		for (int i = 0; i < arr.size(); i++) {

			for (int j = 1; j < (arr.size() - i); j++) {

				T objhigher = arr.get(j);
				T objlower = arr.get(j - 1);

				if (comp.compare(objhigher, objlower) < 0) {
					Collections.swap(arr, j, j - 1);
				}
			}
		}
	}

}
