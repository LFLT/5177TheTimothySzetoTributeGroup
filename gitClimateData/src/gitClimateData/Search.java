package gitClimateData;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Search {

	/**
	 * Returns the maximum value in a list
	 * 
	 * @param comp
	 *            tells the object what attribute to compare
	 * @param arr
	 *            the list to be searched
	 * 
	 * @return highest value in the list based on comparator
	 */
	public static <T> T searchMax(Comparator<T> comp, List<T> arr) {

		T temp = arr.get(0);

		for (T t : arr) {

			if (comp.compare(t, temp) > 0) {
				temp = t;
			}
		}
		return temp;
	}

	/**
	 * 
	 * Returns the minimum value in a list
	 * 
	 * @param comp
	 *            tells the object what attribute to compare
	 * @param arr
	 *            the list to be searched
	 *            
	 * @return the lowest value in the list based on the comparator
	 */
	public static <T> T searchMin(Comparator<T> comp, List<T> arr) {

		T temp = arr.get(arr.size() - 1);

		for (T t : arr) {

			// if t is smaller than temp then replace temp with t
			if (comp.compare(t, temp) < 0) {
				temp = t;
			}
		}
		return temp;
	}

	/**
	 * searches the list for a specific object
	 * 
	 * @param comp
	 *            compares the specified attributes of the object
	 * @param arr
	 *            the list to be searched
	 *            
	 * @param compareObj
	 *            the object to be compared
	 *            
	 * @return a list of the results
	 */
	public static <T> List<T> searchList(Comparator<T> comp, List<T> arr, T compareObj) {

		List<T> list = new ArrayList<T>();

		for (T obj : arr) {

			if (comp.compare(obj, compareObj) == 0) {
				list.add(obj);
			}
		}
		return list;

	}

	/**
	 * Searches all of the results for the specific object
	 * 
	 * @param comp
	 *            the comparator for the weather data attributes
	 * @param wd
	 *            the weatherdata object to compare
	 * @return
	 */
	static <T> List<WeatherData> searchAll(Comparator<WeatherData> comp, WeatherData wd) {

		List<WeatherData> list = new ArrayList<WeatherData>();

		for (Locations loc : ClimateData.locationsArray)
			list.addAll(Search.searchList(comp, loc.weatherArray, wd));

		return list;
	}

}
